import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-addposts',
  templateUrl: './addposts.component.html',
  styleUrls: ['./addposts.component.css']
})
export class AddpostsComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private postservice: PostsService) { }
  postForm !:FormGroup;
  ngOnInit(): void {
    this.postForm = this.formBuilder.group(
      {

        id: [null],
        title: [null],
        body: [null],
        userId: [null]

      }
    );
  }

  public addPost(){
    this.postservice.addPosts(this.postForm.value).subscribe(data=>{

    },
    error => {
    console.log(error)  
  },
    () => {
    console.log("élement ajouté avec succées" );
    });


    
    
  }

}
