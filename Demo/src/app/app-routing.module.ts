import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddpostsComponent } from './addposts/addposts.component';
import { ShowPostsComponent } from './show-posts/show-posts.component';


const routes: Routes = [
  {path:'posts', component:ShowPostsComponent},
  {path:'post', component:AddpostsComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
