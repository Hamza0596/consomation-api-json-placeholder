import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private httpclient : HttpClient) { }

  public getPosts(): Observable<any>{
    return this.httpclient.get<any>(`https://jsonplaceholder.typicode.com/posts`);
  }

  public getPostsById(id:number): Observable<any>{
    return this.httpclient.get<any>(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }

  public deletePosts(id :number): Observable<any>{
 return this.httpclient.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
  }

  public addPosts(post: FormGroup):Observable<any>{
    return this.httpclient.post<any>(`https://jsonplaceholder.typicode.com/posts`,post);
  }

}
