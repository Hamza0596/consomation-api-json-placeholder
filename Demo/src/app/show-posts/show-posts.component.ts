import { Component, OnInit } from '@angular/core';
import { Posts } from '../Model/Posts';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-show-posts',
  templateUrl: './show-posts.component.html',
  styleUrls: ['./show-posts.component.css']
})
export class ShowPostsComponent implements OnInit {

  constructor(private postsService : PostsService) { }
  posts !: [Posts];
  postId !: Posts; 

  ngOnInit(): void {
  this.getAllPosts();
  this.getPostsById(5);
  }

  public getAllPosts(){
    this.postsService.getPosts().subscribe(data=>{
      this.posts=data;
      
    });

  }


  public getPostsById(id:number){
    this.postsService.getPostsById(id).subscribe(data=>{
      this.postId=data;
    });
  }

  public deletePost(id:number){
    this.postsService.deletePosts(id).subscribe(data => {
    },
    error => {
    console.log(error)  
  },
    () => {
    console.log("élement supprimé avec succées" );
    });
    
  }

}
